﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zoology2
{
    class Animal
    {
        public string Name { get; private set; }
        public double Weight { get; private set; }
        public string Location { get; set; }

        public Animal(string name, double weight)
        {
            Name = name;
            Weight = weight;
            Location = "unknown";
        }
        public Animal(string name, double weight, string location)
        {
            Name = name;
            Weight = weight;
            Location = location;
        }

        public virtual void PrintInfo()
        {
            Console.WriteLine($"Name: {this.Name}\nWeight: {this.Weight}\nLocation: {this.Location}");
        }
    }
}
