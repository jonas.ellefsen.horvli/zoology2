﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zoology2
{
    class BaldEagle : Bird
    {
        public BaldEagle(string name, double weight) : base("Bald Eagle", weight, MovementType.Fly, 160) { }

        public override void Move()
        {
            Console.WriteLine($"{Name} flies away at {Speed}km/h");
        }
    }
}
