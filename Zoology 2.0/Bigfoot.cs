﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zoology2
{
    class Bigfoot : Animal
    {
        public Bigfoot(string name, double weight) : base(name, weight)
        {

        }

        public override void PrintInfo()
        {
            Console.WriteLine($"{this.Name} conceals itself before you can get any info!");
        }
    }
}
