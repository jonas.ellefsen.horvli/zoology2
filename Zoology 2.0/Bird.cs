﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zoology2
{
    abstract class Bird : Animal, IMovement
    {
        public MovementType Type { get; set; }
        public int Speed { get; set; }
        protected Bird(string name, double weight, MovementType type, int speed) : base(name, weight)
        {
            Type = type;
            Speed = speed;
        }

        public override void PrintInfo()
        {
            base.PrintInfo();
            Console.WriteLine($"MovementType: {Type}\nSpeed: {Speed}km/h");
        }

        public abstract void Move();
    }
}
