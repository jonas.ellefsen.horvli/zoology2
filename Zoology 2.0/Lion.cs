﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zoology2
{
    class Lion : Animal
    {
        public int Kills { get; set; }
        public Lion(string name, double weight, string location, int kills = 0) : base(name, weight, location)
        {
            Kills = kills;
        }

        public override void PrintInfo()
        {
            base.PrintInfo();
            Console.WriteLine($"Has killed {Kills} animals.");
        }
    }
}
