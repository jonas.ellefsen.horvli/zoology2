﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zoology2
{
    class Ostrich : Bird
    {
        public Ostrich(double weight) : base("Ostrich", weight, MovementType.Run, 70) { }

        public override void Move()
        {
            Console.WriteLine($"{Name} runs away at {Speed}km/h");
        }
    }
}
