﻿using System;

namespace Zoology2
{
    class Program
    {
        static void Main(string[] args)
        {
            Bigfoot bigfoot = new Bigfoot("Bigfoot", 150.0);
            Animal polarBear = new Animal("Polar bear", 350.0, "Within arctic circle");
            Lion lion = new Lion("Lion", 80.0, "Africa, South-Asia and South-Europe", 10);
            Ostrich ostrich = new Ostrich(50.0);
            BaldEagle baldEagle = new BaldEagle("Eagle", 25.0);

            Animal[] animals = { polarBear, lion, ostrich, bigfoot, baldEagle };

            foreach (Animal animal in animals)
            {
                Console.WriteLine($"---------{animal.GetType().Name.ToUpper()}----------");
                animal.PrintInfo();
                Console.WriteLine("-------------------------\n");
            }
        }
    }

    interface IMovement
    {
        MovementType Type { get; set; }
        int Speed { get; set; }
        void Move();
    }

    enum MovementType
    {
        Run,
        Fly,
        Swim
    }
}
